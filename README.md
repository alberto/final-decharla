# Final DeCharla

# ENTREGA CONVOCATORIA Junio

# ENTREGA DE PRÁCTICA

## Datos

* Nombre:Alberto Sanchez Santos
* Titulación: Doble de teleco y Ade
* Cuenta en laboratorios:alberto
* Cuenta URJC:a.sanchezsa.2018@alumnos.urjc.es
* Video básico (url):https://youtu.be/SNBNmlzHGCo
* Video parte opcional (url):https://youtu.be/LFvNjXxSx5k
* Despliegue (url):http://2albert.pythonanywhere.com/
* Contraseñas:PEPE,JUAN
* Cuenta Admin Site: alberto/blastersoda12

## Resumen parte obligatoria
Antes de comenzar, la app pedirá una contraseña de acceso al usuario para poder acceder a la página. Una vez ha accedido con una contraseña válida, no tendrá que volver a autentificarse gracias a una cookie de sesión. En todas las páginas se presenta la cabecera, el menú con los recursos disponibles, un formulario para acceder a una sala (creada o no) y un pie de página con estadísticas de las salas. En la página principal se muestra un listado de todas las salas creadas, sus mensajes y mensajes desde la última visita. En la página de cada sala se puede comentar un texto o imagen, que aparecerán debajo del formulario ordenados de más reciente a menos, así como un botón para acceder a la página en formato JSON. En la página de configuración se podrá cambiar el nombre del charlador, y el estilo y tamaño de letra para mostrar los párrafos y mensajes. En la página de ayuda se muestran detalles de la autoría de la práctica y una breve descripción de su funcionamiento. Adicionalmente, también existe un recurso de sala dinámica, donde los mensajes se actualizan gracias a un script de Javascript y un recurso donde se podrá realizar un PUT de un archivo XML para actualizar los mensajes de una sala
## Lista partes opcionales
-Adición del favicon.ico: Se ha añadido a la app un favicon para mostrar en navegadores.
-Adición de un recurso logout: Se ha añadido un recurso logout que permite dar de baja al usuario que se ha logeado y redirigirle a la página de login de nuevo.
