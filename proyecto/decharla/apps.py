from django.apps import AppConfig


class DecharlaConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "decharla"
