from django.db import models
from django.utils import timezone

class Sala(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return "El nombre de la sala es: " + str(self.nombre)


class Clave(models.Model):
    clave=models.TextField()
    def __str__(self):
        return "La clave es :"
class Usuario(models.Model):
    escalas=[
        ('pequeño','small'),
        ('mediano','medium',),
        ('grande','large')
    ]
    fuente_letra=[
        ('Arial','Arial'),
        ('Times New Roman','Times New Roman'),
        ('Calibri','Calibri'),
        ('Impact','Impact')
    ]
    nombre=models.CharField(max_length=30,default="Anonimo")
    cookie_id=models.TextField()
    ultima_conexion=models.DateTimeField()
    escala_letra=models.CharField(max_length=30,choices=escalas,default='small')
    tipo_letra=models.CharField(max_length=30,choices=fuente_letra,default='Arial')

    def __str__(self):
        return "El usuario es: " + str(self.nombre) + " con Cookie_Id: " + str(self.cookie_id) + \
               " y conectado por última vez: " + str(self.ultima_conexion)


class Mensaje(models.Model):
    sala = models.ForeignKey(Sala, on_delete=models.CASCADE)
    autor = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    texto = models.TextField()
    imagen = models.BooleanField(default=False)
    fecha = models.DateTimeField()

    def __str__(self):
        return "Autor: " + self.autor.nombre + ", en sala " + self.sala.nombre


class Visita(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    sala= models.ForeignKey(Sala, on_delete=models.CASCADE)
    ultima_conexion = models.DateTimeField(default=timezone.now)

    def __str__(self):
       return "La visita es del usuario"+self.usuario.nombre + "en la sala" +self.sala.nombre
