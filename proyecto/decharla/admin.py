from django.contrib import admin
from .models import Sala,Mensaje,Usuario,Clave

admin.site.register(Sala)
admin.site.register(Mensaje)
admin.site.register(Usuario)
admin.site.register(Clave)