from django.shortcuts import render
from django.http import JsonResponse, Http404 ,HttpResponseNotAllowed, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Sala,Mensaje,Usuario,Clave,Visita
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.db import models
from django.utils import timezone
import uuid
from xml.dom.minidom import parse
import xml.etree.ElementTree as ET
# Create your views here.

def contador_pie_pagina():
    salas_totales=Sala.objects.all().count()
    fotos_totales=Mensaje.objects.filter(imagen=True).count()
    mensajes_totales= Mensaje.objects.filter(imagen=False).count()
    return salas_totales,mensajes_totales,fotos_totales

def actualizar_visita_sala(cookie_id,sala_id):
    usuario = Usuario.objects.get(cookie_id=cookie_id)#No es necesario controlar si existe controlado fuera
    sala=Sala.objects.get(nombre=sala_id)#No es necesario controlar si existe controlado fuera
    try:
        visita=Visita.objects.get(usuario=usuario,sala=sala)
        visita.ultima_conexion=timezone.now()
        visita.save()
    except Visita.DoesNotExist:
        visita=Visita(usuario=usuario,sala=sala,ultima_conexion=timezone.now())
        visita.save()

@csrf_exempt
def index(request):
    if "cookie_id" in request.COOKIES:
        cookie_id=request.COOKIES.get("cookie_id")
    else:
        return redirect("/login/")
    usuario=Usuario.objects.get(cookie_id=cookie_id)
    usuario.ultima_conexion=timezone.now()
    usuario.save()
    if request.method in ["GET", "POST"]:
        if request.method=="POST":
            try:
                nombre_sala=request.POST["sala"]
                Sala.objects.get(nombre=nombre_sala)
                redireccion = "sala/" + nombre_sala
                return redirect(redireccion)
            except Sala.DoesNotExist:
                sala = Sala(nombre=nombre_sala)
                sala.save()
                redireccion = "sala/" + nombre_sala
                return redirect(redireccion)
        if request.method == "GET":
            salas = Sala.objects.all()
            usuario = Usuario.objects.get(cookie_id=cookie_id)
            salas_totales, mensajes_totales, fotos_totales=contador_pie_pagina()
            contexto = {
                'usuario': usuario,
                'salas': salas,
                'salas_totales':salas_totales,
                'mensajes_totales':mensajes_totales,
                'fotos_totales':fotos_totales,
            }
            return render(request, 'decharla/index.html', contexto)
    else:
        return HttpResponseNotAllowed(['POST','GET'])
@csrf_exempt
def sala(request, sala_id):
    if "cookie_id" in request.COOKIES:
        cookie_id = request.COOKIES.get("cookie_id")
    else:
        return redirect("/login/")
    usuario = Usuario.objects.get(cookie_id=cookie_id)
    usuario.ultima_conexion = timezone.now()
    usuario.save()
    try:#Comprobar si la sala existe
        sala=Sala.objects.get(nombre=sala_id)#haces una busqueda,necesitas el tipo sala para buscar en la foreign key
    except Sala.DoesNotExist:
        return Http404("La sala no existe")
    actualizar_visita_sala(cookie_id,sala_id)
    if request.method in ["PUT","POST"]:#procesar solicitudes put y post
       if request.method=="POST":
           action=request.POST["action"]
           if action=="enviar mensaje":
               texto=request.POST["texto"]
               if 'es_imagen' in request.POST:
                   # La casilla de verificación está seleccionada
                   imagen = True
               else:
                   # La casilla de verificación no está seleccionada
                   imagen = False
               fecha = timezone.now()
               autor=Usuario.objects.get(cookie_id=cookie_id)
               mensaje=Mensaje(sala=sala,autor=autor,texto=texto,imagen=imagen,fecha=fecha)
               mensaje.save()
           elif action=="enviar sala":
               try:
                   nombre_sala = request.POST["sala"]
                   Sala.objects.get(nombre=nombre_sala)
                   redireccion = '/sala/'+nombre_sala
                   return redirect(redireccion)
               except Sala.DoesNotExist:
                   sala = Sala(nombre=nombre_sala)
                   sala.save()
                   redireccion = '/sala/'+nombre_sala
                   return redirect(redireccion)
           elif action=="online":
               redireccion = '/sala/'+sala_id+"/din"
               return redirect(redireccion)
           elif action=="json":
               redireccion = '/sala/' + sala_id + "/json"
               return redirect(redireccion)
    salas_totales, mensajes_totales, fotos_totales = contador_pie_pagina()
    try:#procesar GET
        mensajes = Mensaje.objects.filter(sala=sala)
    except Mensaje.DoesNotExist:
        usuario = Usuario.objects.get(cookie_id=cookie_id)
        contexto = {
            'sala': sala,
            'usuario': usuario,
            'salas_totales': salas_totales,
            'mensajes_totales': mensajes_totales,
            'fotos_totales': fotos_totales
        }
        return render(request, 'decharla/sala.html', contexto)
    usuario = Usuario.objects.get(cookie_id=cookie_id)
    contexto = {
        'mensajes': mensajes,
        'sala':sala,
        'usuario': usuario,
        'salas_totales': salas_totales,
        'mensajes_totales': mensajes_totales,
        'fotos_totales': fotos_totales
    }
    return render(request, 'decharla/sala.html', contexto)


def loggedIn(request):
    if request.method in ["GET","POST"]:
        if request.method=="POST":
            # Obtener la contraseña ingresada en el formulario
            clave = request.POST['clave']
            try:
                Clave.objects.get(clave=clave)
                cookie_id = str(uuid.uuid4())# Generar un UUID único
                ultima_conexion=timezone.now()
                usuario=Usuario(cookie_id=cookie_id,ultima_conexion=ultima_conexion)
                usuario.save()
                response =redirect('/')
                response.set_cookie('cookie_id', cookie_id)
                return response
            except Clave.DoesNotExist:
                contexto={
                    'error':'contraseña incorrecta',
                }
                return render(request, 'decharla/login.html',contexto,status=401)

        else:#GET
            if "cookie_id" in request.COOKIES:
                cookie_id = request.COOKIES.get("cookie_id")
                return redirect('/')
            else:
                return render(request, 'decharla/login.html')
    else:
        return HttpResponseNotAllowed(['POST','GET'])

def logout_vista(request):
    if request.method=="GET":
        if "cookie_id" in request.COOKIES:
            cookie_id = request.COOKIES.get("cookie_id")
            response = redirect('/login')
            response.delete_cookie("cookie_id")
            usuario = Usuario.objects.get(cookie_id=cookie_id)
            usuario.delete()
            return response
        else:#No deberia pasar pero si borra la cookie mientras esta en la pagina
            return redirect("/login/")
    else:
        return HttpResponseNotAllowed('GET')

def configuracion(request):
    if "cookie_id" in request.COOKIES:
        cookie_id = request.COOKIES.get("cookie_id")
    else:
        return redirect("/login/")
    usuario = Usuario.objects.get(cookie_id=cookie_id)
    usuario.ultima_conexion = timezone.now()
    usuario.save()
    if request.method in ["GET", "POST"]:
        if request.method=="POST":
            action = request.POST["action"]
            if action=="enviar nombre":
                nombre=request.POST["nombre"]
                usuario.nombre=nombre
                usuario.save()
            elif action=="enviar tipografia":
                tipo_letra = request.POST["tipo letra"]
                escala_letra = request.POST["escala letra"]
                usuario.tipo_letra = tipo_letra
                usuario.escala_letra = escala_letra
                usuario.save()
            elif action == "enviar sala":
                try:
                    nombre_sala = request.POST["sala"]
                    Sala.objects.get(nombre=nombre_sala)
                    redireccion = '/sala/' + nombre_sala
                    return redirect(redireccion)
                except Sala.DoesNotExist:
                    sala = Sala(nombre=nombre_sala)
                    sala.save()
                    redireccion = '/sala/' + nombre_sala
                    return redirect(redireccion)
    salas_totales, mensajes_totales, fotos_totales = contador_pie_pagina()
    contexto={
        'usuario':usuario,
        'salas_totales': salas_totales,
        'mensajes_totales': mensajes_totales,
        'fotos_totales': fotos_totales
    }
    return render(request, 'decharla/configuracion.html', contexto)
@csrf_exempt
def sala_json(request,sala_id):
    if "cookie_id" in request.COOKIES:
        cookie_id = request.COOKIES.get("cookie_id")
    else:
        return redirect("/login/")
    usuario = Usuario.objects.get(cookie_id=cookie_id)
    usuario.ultima_conexion = timezone.now()
    usuario.save()
    try:#Comprobar si la sala existe
        sala=Sala.objects.get(nombre=sala_id)#haces una busqueda,necesitas el tipo sala para buscar en la foreign key
    except Sala.DoesNotExist:
        return Http404("La sala no existe")
    actualizar_visita_sala(cookie_id,sala_id)
    if request.method=="GET":
        try:
            sala=Sala.objects.get(nombre=sala_id)
        except Sala.DoesNotExist:
            sala = Sala(nombre=sala_id)
            sala.save()
        mensaje_lista=Mensaje.objects.filter(sala=sala)
        mensajes_data=[]
        for mensaje in mensaje_lista:
            mensaje_json= {
                "author": mensaje.autor.nombre,
                "text": mensaje.texto,
                "isimg": mensaje.imagen,
                "date": mensaje.fecha
            }
            mensajes_data.append(mensaje_json)
        return JsonResponse(mensajes_data, safe=False, json_dumps_params={'indent': 2})
    else:
        return HttpResponseNotAllowed(['GET'])
def get_favicon(request):
    with open("favicon.jpg", "rb") as favicon:
        ans = favicon.read()
    return HttpResponse(ans)
@csrf_exempt
def sala_din(request,sala_id):
    if "cookie_id" in request.COOKIES:
        cookie_id = request.COOKIES.get("cookie_id")
    else:
        return redirect("/login/")
    usuario = Usuario.objects.get(cookie_id=cookie_id)
    usuario.ultima_conexion = timezone.now()
    usuario.save()
    try:#Comprobar si la sala existe
        sala=Sala.objects.get(nombre=sala_id)#haces una busqueda,necesitas el tipo sala para buscar en la foreign key
    except Sala.DoesNotExist:
        return Http404("La sala no existe")
    actualizar_visita_sala(cookie_id,sala_id)
    if request.method in ["PUT","POST"]:#procesar solicitudes put y post
       if request.method=="POST":
           action=request.POST["action"]
           if action=="enviar mensaje":
               texto=request.POST["texto"]
               if 'es_imagen' in request.POST:
                   # La casilla de verificación está seleccionada
                   imagen = True
               else:
                   # La casilla de verificación no está seleccionada
                   imagen = False
               fecha = timezone.now()
               autor=Usuario.objects.get(cookie_id=cookie_id)
               mensaje=Mensaje(sala=sala,autor=autor,texto=texto,imagen=imagen,fecha=fecha)
               mensaje.save()
           elif action=="enviar sala":
               try:
                   nombre_sala = request.POST["sala"]
                   Sala.objects.get(nombre=nombre_sala)
                   redireccion = '/sala/'+nombre_sala
                   return redirect(redireccion)
               except Sala.DoesNotExist:
                   sala = Sala(nombre=nombre_sala)
                   sala.save()
                   redireccion = '/sala/'+nombre_sala
                   return redirect(redireccion)
           elif action=="normal":
               redireccion = '/sala/'+sala_id
               return redirect(redireccion)
           elif action=="json":
               redireccion = '/sala/' + sala_id + "/json"
               return redirect(redireccion)
    salas_totales, mensajes_totales, fotos_totales = contador_pie_pagina()
    try:#procesar GET
        mensajes = Mensaje.objects.filter(sala=sala)
    except Mensaje.DoesNotExist:
        usuario = Usuario.objects.get(cookie_id=cookie_id)
        contexto = {
            'sala': sala,
            'usuario': usuario,
            'salas_totales': salas_totales,
            'mensajes_totales': mensajes_totales,
            'fotos_totales': fotos_totales
        }
        return render(request, 'decharla/sala_dinamica.html', contexto)
    usuario = Usuario.objects.get(cookie_id=cookie_id)
    contexto = {
        'mensajes': mensajes,
        'sala':sala,
        'usuario': usuario,
        'salas_totales': salas_totales,
        'mensajes_totales': mensajes_totales,
        'fotos_totales': fotos_totales
    }
    return render(request, 'decharla/sala_dinamica.html', contexto)
def xml_parser(xml_string):
    root = ET.fromstring(xml_string)
    messages = []
    for msg in root.findall('message'):
        isimg = msg.get('isimg')
        if isimg == "True":
            is_img = True
        else:
            is_img = False
        text = msg.find('text').text
        messages.append({'isimg': is_img, 'text': text})
    return messages

@csrf_exempt
def sala_xml(request,sala_id):
    if request.method=="PUT":
        clave = request.META.get("HTTP_autorization")
        if "cookie_id" in request.COOKIES:
            cookie_id = request.COOKIES.get("cookie_id")
            usuario=Usuario.objects.get(cookie_id=cookie_id)
        else:
            try:
                Clave.objects.get(clave=clave)
                cookie_id = str(uuid.uuid4())  # Generar un UUID único
                ultima_conexion = timezone.now()
                usuario = Usuario(cookie_id=cookie_id, ultima_conexion=ultima_conexion)
                usuario.save()
            except Clave.DoesNotExist:
                contexto = {
                    'error': 'contraseña incorrecta',
                }
                return render(request, 'decharla/login.html', contexto, status=401)
        try:
            sala=Sala.objects.get(nombre=sala_id)
        except Sala.DoesNotExist:
            sala = Sala(nombre=sala_id)
            sala.save()

        xml_string = request.body.decode('utf-8')
        msg_xml = xml_parser(xml_string)
        for msg in msg_xml:
            mensajes = Mensaje(autor=usuario,sala=sala, texto=msg['text'], imagen=msg['isimg'],fecha=timezone.now())
            mensajes.save()
        return redirect("/sala/" + str(sala.nombre))
    else:
        return HttpResponseNotAllowed(['PUT'])


def ayuda(request):
    return render(request, 'decharla/ayuda.html')