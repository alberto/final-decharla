from django.urls import path

from . import views

urlpatterns=[
	path('',views.index,name='index'),
	path('login/', views.loggedIn),
	path('favicon.ico', views.get_favicon, name='favicon'),
	path('logout/', views.logout_vista),
	path('sala/<str:sala_id>',views.sala,name="sala"),
	path('sala/<str:sala_id>/json',views.sala_json),
	path('sala/<str:sala_id>/din',views.sala_din),
	path('sala/<str:sala_id>/xml',views.sala_xml),
	path('configuracion/',views.configuracion),
	path('ayuda/',views.ayuda),


]